/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.81794017514156, "KoPercent": 0.1820598248584485};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9092704862817922, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5085382513661202, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9994647039143526, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9497211571976298, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7574236721037223, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.052884615384615384, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.8470160116448326, 500, 1500, "me"], "isController": false}, {"data": [0.8413030484160191, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.6406429391504018, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9398211160801089, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.3352380952380952, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 54927, 100, 0.1820598248584485, 276.4216505543731, 11, 9240, 75.0, 868.0, 1187.9500000000007, 1761.0, 180.22620559312526, 821.5271990826188, 219.54957662841613], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1464, 10, 0.6830601092896175, 1037.8504098360654, 155, 9240, 1041.0, 1375.5, 1530.75, 1845.4499999999994, 4.80407952983025, 3.032121436728238, 5.9441101213817635], "isController": false}, {"data": ["getLatestMobileVersion", 29890, 10, 0.03345600535296086, 50.55078621612623, 11, 9225, 29.0, 94.0, 145.0, 227.0, 98.0765318510838, 65.60335016545697, 72.31228666754714], "isController": false}, {"data": ["findAllConfigByCategory", 5738, 10, 0.17427675148135238, 264.6115371209475, 34, 9232, 208.0, 497.0, 645.0500000000002, 894.0, 18.827806616310433, 21.271958989719845, 24.454084765325074], "isController": false}, {"data": ["getNotifications", 2391, 10, 0.41823504809703055, 635.3203680468424, 82, 9231, 463.0, 1153.0, 1237.0, 1412.3199999999997, 7.846135671532081, 64.97332104161471, 8.727293486206094], "isController": false}, {"data": ["getHomefeed", 728, 10, 1.3736263736263736, 2087.7472527472496, 605, 9224, 1975.0, 2704.2, 2890.6499999999996, 3645.9100000000008, 2.388945221617329, 28.725897249103323, 11.954057925358587], "isController": false}, {"data": ["me", 3435, 10, 0.29112081513828236, 442.1935953420657, 69, 9235, 332.0, 865.4000000000001, 976.3999999999996, 1163.0, 11.271940906808776, 14.805747449046562, 35.72016429940868], "isController": false}, {"data": ["findAllChildrenByParent", 3346, 10, 0.2988643156007173, 454.07531380753085, 59, 9229, 316.0, 953.0, 1071.6499999999996, 1228.5299999999997, 10.979887707185494, 12.311540828791196, 17.542086219683075], "isController": false}, {"data": ["getAllClassInfo", 1742, 10, 0.574052812858783, 872.3157290470716, 100, 9201, 926.5, 1354.4, 1401.6999999999998, 1522.1399999999999, 5.717080022710788, 16.339577313087254, 14.677932987994787], "isController": false}, {"data": ["findAllSchoolConfig", 5143, 10, 0.19443904335990667, 295.25374295158406, 41, 9236, 237.0, 547.0, 688.0, 937.5600000000004, 16.875186617973732, 367.3259702801648, 12.376235498142844], "isController": false}, {"data": ["getChildCheckInCheckOut", 1050, 10, 0.9523809523809523, 1446.6942857142892, 542, 9227, 1428.0, 1646.0, 1724.35, 2127.4000000000005, 3.4455827628979647, 227.17648156982392, 15.855064432397667], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 100, 100.0, 0.1820598248584485], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 54927, 100, "502/Bad Gateway", 100, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getCountCheckInOutChildren", 1464, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getLatestMobileVersion", 29890, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllConfigByCategory", 5738, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getNotifications", 2391, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getHomefeed", 728, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["me", 3435, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllChildrenByParent", 3346, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllClassInfo", 1742, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findAllSchoolConfig", 5143, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildCheckInCheckOut", 1050, 10, "502/Bad Gateway", 10, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
